var myapp=angular.module("myApp",["ngRoute"]);

  //RouteProvider decides which template to load depending on thee view
  myapp.config(function($routeProvider) {
      $routeProvider
      .when("/shview", {
        templateUrl : "views/shview.htm",
        controller:"loaddata"
      })
      .when("/", {
        templateUrl : "views/allhview.htm",
    })
  });
  
  // loads all jsondata and push it into an array
 
  var housearray = [];
  myapp.controller("loaddata", function($q,$scope,$http,){
  //loaded data was unordered 
    /*   for(var i = 1; i < 10; i++){
      $http({method:'GET', url:'https://www.anapioficeandfire.com/api/houses?page='+i+'&pageSize=50' })
     .then(function(response){
     Array.prototype.push.apply(housearray, response.data);
    });
 }
 */

    // loads all jsondata and push it into an array
    var promises = [];
      for(var i = 1; i < 10; i++){
        promises.push($http.get('https://www.anapioficeandfire.com/api/houses?page='+i+'&pageSize=50'));
      }
      $q.all(
      promises).then(function(results) {
        for(x in results) {
          Array.prototype.push.apply(housearray, results[x].data);
      }
    });
    
    $scope.houses= housearray;


    //loads data from array of urls
    function load_array (urlarray) {
      var noms = [];
      var arraylength = urlarray.length;
      for(var i = 0; i < arraylength; i++){
        $http({method:'GET', url:urlarray[i] })
        .then(function(response){
        noms.push(response.data);
        });
      }
      return noms;
    }
     
      //loads data for simple url
      //did not work as expected
     /*  function simple_load (spi){
            $http({method:'GET', url:spi })
            .then(function(response){
            $scope.info = response.data;
             });
            return  $scope.info;
          
          } */
  
  // loads clicked house with corresponding information
    $scope.loadName = function (hou) {

    //Gets the json from the server 
    $http({method:'GET', url:hou })
     .then(function(response){
      $scope.test = response.data;
    
      //Gets json of the internal urls 
      if($scope.test.cadetBranches.length  > 0){
        $scope.cadetBranches= load_array($scope.test.cadetBranches);
      }else{
        $scope.cadetBranches=[{name:"No information available"}];
      }
      if($scope.test.swornMembers.length  > 0){
        $scope.nami= load_array($scope.test.swornMembers);
       
      }else{
        $scope.nami=[{name:"No information available"}];
      }
      if($scope.test.overlord){
        $http({method:'GET', url: $scope.test.overlord })
        .then(function(response){
        $scope.over = response.data;
        }); 
       // $scope.over= simple_load($scope.test.overlord)
      } else{
        $scope.over = {name:"No information available"};
      }
      if($scope.test.currentLord){
        $http({method:'GET', url: $scope.test.currentLord })
        .then(function(response){
        $scope.curr = response.data;
        });
        //$scope.curr= simple_load($scope.test.currentLord);
      }  else{
        $scope.curr = {name:"No information available"};
      } 
      if($scope.test.founder){
        $http({method:'GET', url: $scope.test.founder })
        .then(function(response){
        $scope.foun = response.data;
        });
        //$scope.foun= simple_load($scope.test.founder);
      } else{
        $scope.foun = {name:"No information available"};
      }
      if($scope.test.heir){
        $http({method:'GET', url: $scope.test.heir })
        .then(function(response){
        $scope.hei = response.data;
        });
       // $scope.hei = simple_load($scope.test.heir);
      } else{
        $scope.hei = {name:"No information available"};
      }
      
      //replaces empyts String with "No information available"
      $scope.test = JSON.stringify($scope.test).replace(/""/g, '"No information available"');
      $scope.test = JSON.parse($scope.test);
      
      //changes array with informations into Strings
      $scope.titles = $scope.test.titles.toString();
      $scope.seats = $scope.test.seats.toString();
      $scope.weapons = $scope.test.ancestralWeapons.toString();
    
    });
    }
 
   });

